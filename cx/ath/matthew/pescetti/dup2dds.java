/* 
 * Pescetti Pseudo-Duplimate Generator
 * 
 * Copyright (C) 2007 Matthew Johnson
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License Version 2 as published by
 * the Free Software Foundation.  This program is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * To Contact me, please email src@matthew.ath.cx
 *
 */

package cx.ath.matthew.pescetti;

import cx.ath.matthew.debug.Debug;

import static cx.ath.matthew.pescetti.Gettext._;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintStream;

public enum dup2dds
{
   start_row,
   north,
   south,
   east,
   west;
   public static void syntax()
   {
      System.out.println(_("Usage: dup2dds <dupfile>"));
   }
   public static String getState(dup2dds state)
   {
      switch (state) {
         case start_row:
            return "start_row";
         case north:
            return "north";
         case south:
            return "south";
         case east:
            return "east";
         case west:
            return "west";
      }
      return null;
   }
   public static void main(String[] args)

   {
      if (1 != args.length) {
         syntax();
         System.exit(1);
      }
      try {
         File input = new File(args[0]);
         File output = new File(input.getCanonicalPath().replaceAll("\\.[^\\.]*$",".dds"));
         if (Debug.debug) Debug.print(Debug.INFO, "Converting "+input+" to "+output);

         InputStream is = new FileInputStream(input);
         PrintStream os = new PrintStream(new FileOutputStream(output));

         dup2dds state = start_row;
         int c;
         StringBuffer deal = null;
         StringBuffer deal2 = null;
         while (-1 != (c = is.read())) {
            if (Debug.debug) Debug.print(Debug.DEBUG, "State: "+getState(state));
            if (Debug.debug) Debug.print(Debug.DEBUG, "c: "+((char) c));
            switch (state) {
               case start_row:
                  if (c == '') {
                     state = north;
                     deal = new StringBuffer();
                     deal2 = new StringBuffer();
                  }
                  break;
               case north:
                  if (c == '' || c == '' || c == '')
                     deal.append('.');
                  else if (c == '') {
                     deal.append(' ');
                     state = east;
                  } else deal.append((char) c);
                  break;
               case south:
                  if (c == '' || c == '' || c == '')
                     deal.append('.');
                  else if (c == '') {
                     deal.append(' ');
                     state = west;
                  } else deal.append((char) c);
                  break;
               case east:
                  if (c == '' || c == '' || c == '')
                     deal.append('.');
                  else if (c == '') {
                     deal.append(' ');
                     state = south;
                  } else deal.append((char) c);
                  break;
               case west:
                  if (c == '' || c == '' || c == '')
                     deal2.append('.');
                  else if (c == 'Y') {
                     os.println(deal2.toString()+' '+deal.toString());
                     state = start_row;
                  } else deal2.append((char) c);
                  break;
            }
         }

         is.close();
         os.close();
      } catch (IOException IOe) {
         if (Debug.debug) Debug.print(IOe);
         System.err.println(_("Error occurred while converting files: "+IOe.getMessage()));
         System.exit(1);
      }
   }
}


